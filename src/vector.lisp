(in-package #:montana)

(declaim (optimize (speed 3)
		   (safety 0)
		   (space 3)))
;; (declaim (inline make-vec2))
;; (declaim (inline make-vec3))
;; (declaim (inline make-vec4))

;; (defstruct (vec2 :copier
;; 		 (:type (vector single-float)))
;;   (x 0.0 :type single-float)
;;   (y 0.0 :type single-float))
;; (defstruct (vec3 (:include vec2)
;; 		 :copier
;; 		 (:type (vector single-float)))
;;   (z 0.0 :type single-float))
;; (defstruct (vec4 (:include vec3)
;; 		 :copier
;; 		 (:type (vector single-float)))
;;   (w 0.0 :type single-float))

(defclass vec () ())
(defclass vec2 (vec)
  ((x :initarg :x
      :initform 0.0
      :reader vec-x
      :type single-float)
   (y :initarg :y
      :initform 0.0
      :reader vec-y
      :type single-float)))
(defclass vec3 (vec2)
  ((z :initarg :z
      :initform 0.0
      :reader vec-z
      :type single-float)))
(defclass vec4 (vec3)
  ((w :initarg :w
      :initform 0.0
      :reader vec-w
      :type single-float)))

(defun make-vec2 (x y)
  (make-instance 'vec2 :x x :y y))

(defun make-vec3 (x y z)
  (make-instance 'vec3 :x x :y y :z z))

(defun make-vec4 (x y z w)
  (make-instance :vec4 :x x :y y :z z :w w))

(defgeneric vector-div (vec val)
  (:documentation "Divide a vector"))

(defmethod vector-div ((vec vec2) val)
  (make-vec2 (/ (vec-x vec) val)
	     (/ (vec-y vec) val)))

(defmethod vector-div ((vec vec3) val)
  (make-vec3 (/ (vec-x vec) val)
	     (/ (vec-y vec) val)
	     (/ (vec-z vec) val)))

(defmethod vector-div ((vec vec4) val)
  (make-vec4 (/ (vec-x vec) val)
	     (/ (vec-y vec) val)
	     (/ (vec-z vec) val)
	     (/ (vec-w vec) val)))

(defgeneric vector-floor (vec)
  (:documentation "Floor the vector"))

(defmethod vector-floor ((vec vec2))
  (make-vec2 (truncate (vec-x vec))
	     (truncate (vec-y vec))))

(defgeneric vector-dot (vec x y)
  (:documentation ""))

(defmethod vector-dot ((vec vec2) (x single-float) (y single-float))
  (+ (* (vec-x vec) x)
     (* (vec-y vec) y)))

(defgeneric vector-length (vec)
  (:documentation "Get the vector length"))

(defmethod vector-length ((vec vec2))
  (let ((x (vec-x vec))
	(y (vec-y vec)))
    (+ (* x x)
       (* y y))))

(defmethod vector-length ((vec vec3))
  (let ((x (vec-x vec))
	(y (vec-y vec))
	(z (vec-z vec)))
    (+ (* x x)
       (* y y)
       (* z z))))

(defmethod vector-length ((vec vec4))
  (let ((x (vec-x vec))
	(y (vec-y vec))
	(z (vec-z vec))
	(w (vec-w vec)))
    (+ (* x x)
       (* y y)
       (* z z)
       (* w w))))

(defgeneric vector-normalize (vec)
  (:documentation "Normalize a vector"))

(defmethod vector-normalize ((vec vec))
  (let ((length (vector-length vec)))
    (when (> length 0)
	(vector-div vec length))))
