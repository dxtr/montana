(in-package #:montana)

(require :alexandria)
(require :sdl2)
(require :cl-opengl)

(defparameter *window* nil)
(defparameter *renderer* nil)

(defvar *font-path* "/usr/share/fonts/noto/NotoSans-Regular.ttf")
(defvar *font* nil)

(defvar *debug* nil)

;; Timer stuff
(defclass timer ()
  ((start-tick :reader timer-start-tick
		:writer (setf start-tick)
		:initform 0
		:documentation "The tick the timer was started")
   (pause-tick :reader timer-paused-tick
		 :writer (setf pause-tick)
		 :initform 0
		 :documentation "The tick the timer was paused")
   (paused :reader timer-is-paused
	   :writer (setf paused)
	   :initform nil
	   :documentation "A boolean indicating if the timer is paused")
   (started :reader timer-is-started
	    :writer (setf started)
	    :initform nil
	    :documentation "A boolean indicating if the timer is started")))

(defmethod timer-start ((tmr timer))
  (setf (started tmr) t)
  (setf (paused tmr) nil)
  (setf (pause-tick tmr) 0)
  (setf (start-tick tmr) (sdl2:get-ticks)))

(defmethod timer-stop ((tmr timer))
  (setf (started tmr) nil)
  (setf (paused tmr) nil)
  (setf (pause-tick tmr) 0)
  (setf (start-tick tmr) 0))

(defmethod timer-pause ((tmr timer))
  (when (and (timer-is-started tmr) (not (timer-is-paused tmr)))
    (setf (paused tmr) t)
    (setf (pause-tick tmr) (- (sdl2:get-ticks) (timer-start-tick tmr)))
    (setf (start-tick tmr) 0)))

(defmethod timer-unpause ((tmr timer))
  (when (and (timer-is-started tmr) (timer-is-paused tmr))
    (setf (paused tmr) nil)
    (setf (start-tick tmr) (- (sdl2:get-ticks) (timer-pause-tick tmr)))
    (setf (pause-tick tmr) 0)))

(defmethod get-ticks ((tmr timer))
  (if (timer-is-started tmr)
      (if (timer-is-paused tmr)
	  (timer-paused-tick tmr)
	  (- (sdl2:get-ticks) (timer-start-tick tmr)))
      0))

(defparameter *fps-counter-timer* (make-instance 'timer))
(defparameter *fps-cap-timer* (make-instance 'timer))

;; TODO:
;; Make a proper window class with all the relevant methods
;; Also break this out to its own file/package?
(defclass window ()
  ((handle :accessor window-handle
	   :initarg handle)
   (renderer :accessor window-renderer)
   (width :accessor window-width
	  :initform 0)
   (height :accessor window-height
	   :initform 0)
   (mouse-focus :accessor has-mouse-focus
		:initform nil)
   (keyboard-focus :accessor has-keyboard-focus
		   :initform nil)
   (minimized :accessor is-minimized
	      :initform nil)))

(defmethod make-window ((w window))
  (let (wh 
	win (make-instance 'window ))))

;; TODO:
;; Make a proper renderer class with all the relevant methods
;; Break it out to own file/package?
(defclass renderer ()
  ())

(defclass texture ()
  ((renderer :initarg :renderer
	     :initform (error "Must supply a renderer"))
   (width :accessor texture-width
	  :initform 0)
   (height :accessor texture-height
	   :initform 0)
   (texture :accessor texture-texture
	    :initform nil)))

(defun load-texture-from-text (renderer text)
  (let ((tex (make-instance 'texture :renderer renderer)))
    (with-slots (renderer texture width height) tex
      (let ((surface (sdl2-ttf:render-text-solid *font* text 0 0 0
						 0)))
	(setf width (sdl2:surface-width surface))
	(setf height (sdl2:surface-height surface))
	(sdl2:set-color-key surface :true (sdl2:map-rgb
					   (sdl2:surface-format surface)
					   0 #xFF #xFF))
	(setf texture (sdl2:create-texture-from-surface renderer
							surface))))
    tex))

(defun debug-log (msg &rest args)
  (apply #'format t msg args)
  (finish-output))

(defun log-keysym (msg keysym)
  (let* ((scancode (sdl2:scancode-value keysym))
	 (scancode-name (sdl2:scancode-name scancode))
	 (scancode-key-name (sdl2:scancode-key-name scancode)))
    (debug-log msg scancode-name scancode-key-name (sdl2:get-key-from-scancode scancode) scancode (sdl2:mod-value keysym))))

(defun set-color (txtr r g b)
  (sdl2:set-texture-color-mod (texture-texture txtr) r g b))

(defun render (txtr x y &key clip angle center (flip :none))
  (with-slots (renderer texture width height) txtr
    (sdl2:render-copy-ex renderer
			 texture
			 :source-rect clip
			 :dest-rect (sdl2:make-rect x y
						    (if clip
							(sdl2:rect-width clip) width)
						    (if clip
							(sdl2:rect-height
							 clip) height))
			 :angle angle
			 :center center
			 :flip (list flip))))

(defun push-quit-event ()
  (sdl2:push-quit-event))

(defun quit ()
  (sdl2-ttf:quit)
  (sdl2-image:quit)
  (sdl2:quit))

(defmacro with-main (&body body)
  "Enables REPL access via UPDATE-SWANK in the main loop using SDL2. Wrap this around
the sdl2:with-init code."
  `(sdl2:make-this-thread-main
    (lambda ()
      #+sbcl (sb-int:with-float-traps-masked (:invalid) ,@body)
      #-sbcl ,@body)))

(defmacro continuable (&body body)
  "Helper macro that we can use to allow us to continue from an
error. Remember to hit C in slime or pick the restart so errors don't kill the app."
  `(restart-case (progn ,@body) (continue () :report "Continue")))

(defparameter *hash* #(151 160 137 91 90 15 131 13 201 95 96 53 194 233 7 225
		       140 36 103 30 69 142 8 99 37 240 21 10 23 190 6 148
		       247 120 234 75 0 26 197 62 94 252 219 203 117 35 11 32
		       57 177 33 88 237 149 56 87 174 20 125 136 171 168 68 175
		       74 165 71 134 139 48 27 166 77 146 158 231 83 111 229 122
		       60 211 133 230 220 105 92 41 55 46 245 40 244 102 143 54
		       65 25 63 161 1 216 80 73 209 76 132 187 208 89 18 169
		       200 196 135 130 116 188 159 86 164 100 109 198 173 186 3 64
		       52 217 226 250 124 123 5 202 38 147 118 126 255 82 85 212
		       207 206 59 227 47 16 58 17 182 189 28 42 223 183 170 213
		       119 248 152 2 44 154 163 70 221 153 101 155 167 43 172 9
		       129 22 39 253 19 98 108 110 79 113 224 232 178 185 112 104
		       218 246 97 228 251 34 242 193 238 210 144 12 191 179 162 241
		       81 51 145 235 249 14 239 107 49 192 214 31 181 199 106 157
		       184 84 204 176 115 121 50 45 127 4 150 254 138 236 205 93
		       222 114 67 29 24 72 243 141 128 195 78 66 215 61 156 180
		       151 160 137 91 90 15 131 13 201 95 96 53 194 233 7 225
		       140 36 103 30 69 142 8 99 37 240 21 10 23 190 6 148
		       247 120 234 75 0 26 197 62 94 252 219 203 117 35 11 32
		       57 177 33 88 237 149 56 87 174 20 125 136 171 168 68 175
		       74 165 71 134 139 48 27 166 77 146 158 231 83 111 229 122
		       60 211 133 230 220 105 92 41 55 46 245 40 244 102 143 54
		       65 25 63 161 1 216 80 73 209 76 132 187 208 89 18 169
		       200 196 135 130 116 188 159 86 164 100 109 198 173 186 3 64
		       52 217 226 250 124 123 5 202 38 147 118 126 255 82 85 212
		       207 206 59 227 47 16 58 17 182 189 28 42 223 183 170 213
		       119 248 152 2 44 154 163 70 221 153 101 155 167 43 172 9
		       129 22 39 253 19 98 108 110 79 113 224 232 178 185 112 104
		       218 246 97 228 251 34 242 193 238 210 144 12 191 179 162 241
		       81 51 145 235 249 14 239 107 49 192 214 31 181 199 106 157
		       184 84 204 176 115 121 50 45 127 4 150 254 138 236 205 93
		       222 114 67 29 24 72 243 141 128 195 78 66 215 61 156 180))

(defparameter *hashmask* (- (length *hash*) 1))

(defun update-swank ()
  (continuable
   (let ((connection (or swank::*emacs-connection*
			 (swank::default-connection))))
     (when connection
       (swank::handle-requests connection t)))))

(defun handle-keydown (keysym)
  (log-keysym "Key down: ~S/~S/~S/~S/~S~%" keysym)
  (case (sdl2:scancode keysym)
    (:scancode-q (push-quit-event))))

(defun handle-keyup (keysym)
  (log-keysym "Key up: ~S/~S/~S/~S/~S~%" keysym))

(defun handle-windowevent (type data1 data2)
;;  (debug-log "windowevent type: ~S~%" type)
  )

(defun handle-mousemotion (xrel yrel x y state which timestamp)
  )

(defun handle-mousewheel (x y which timestamp)
  )

(defun handle-userevent (data1 data2 code timestamp)
  )

(defun handle-mousebuttondown (x y button timestamp)
  )

(defun handle-mousebuttonup (x y button timestamp)
  )

(defun handle-quit ()
  (quit))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *gradients1D* (vector 1.0 -1.0))
  (defparameter *gradients2D* (vector (vector 1.0 0.0)
				      (vector -1.0 0.0)
				      (vector 0.0 1.0)
				      (vector 0.0 -1.0)
				      (vector ))))

(defun perlin-value-1d (point freq)
  (let ((mulpoint (* point freq))
	(i0 (truncate mulpoint))
	(t0 (- mulpoint i0))
	(t1 (- t0 1))
	(i1 (bit-and i0 *hashmask*))
	(g0 ()))))

(defun perlin-value (x y z freq)
  (flet ((logand-hash (val)
	   (logand (floor (* val freq))
		   *hashmask*)))
    (let ((ix (logand-hash x))
	  (iy (logand-hash y))
	  (iz (logand-hash z)))
      (float (* (elt *hash*
		     (logand (+ (elt *hash* (logand (+ (elt *hash* ix)
						       iy) *hashmask*)) iz) *hashmask*))
		(/ 1 *hashmask*))))))

(defun draw-perlin (renderer)
  (multiple-value-bind (x y) (sdl2:get-renderer-output-size renderer)
    (loop
       for cur_y from 0 below y
       do (loop
	     for cur_x from 0 below x
	     do
	       (let ((pv (perlin-value cur_x cur_y 1 63)))
		 (sdl2:set-render-draw-color renderer
					     (round (* 255 pv))
					     (round (* 255 pv))
					     (round (* 255 pv))
					     #xFF)
		 (sdl2:render-draw-point renderer cur_x cur_y))))))

(defun draw-grid (renderer)
  (multiple-value-bind (x y) (sdl2:get-renderer-output-size renderer)
    (let ((rects (loop
		    for cur_y from 0 below y by 10
		    append (loop for cur_x from 0 below x by 10
			      collect (sdl2:make-rect cur_x cur_y (+ cur_x 10) (+ cur_y 10))))))
      (sdl2:set-render-draw-color renderer #xFF #xFF #xFF #xFF)
      (mapc (lambda (rect) (sdl2:render-draw-rect renderer rect))
	    rects))))

(defparameter *counted-frames* 0)
(defparameter *frame-length* (/ 1000 60))

(defun do-idle (window renderer)
  (timer-start *fps-cap-timer*)
  (let* ((delay (round *frame-length*))
	 (ticks (get-ticks *fps-counter-timer*))
	 (avg-fps (if (> ticks 0)
		      (float (/ (incf *counted-frames*)
				(/ ticks 1000)))
		      2.0)))
    (declare ((unsigned-byte 32) delay))
    (sdl2:set-render-draw-color renderer #x00 #x00 #x00 #x00)
    (sdl2:render-clear renderer)
    ;;(format t "Ticks: ~S~%" (get-ticks *fps-counter-timer*))
    (draw-perlin renderer)
    (sdl2:render-present renderer)
    (update-swank)
    ;; (format t "Cap start tick: ~S~%" (timer-start-tick
    ;; 				      *fps-cap-timer*))
    ;; (format t "Current tick: ~S~%" (sdl2:get-ticks))
    ;; (format t "Ticks: ~S~%" (get-ticks *fps-cap-timer*))
    (let ((frame-ticks (get-ticks *fps-cap-timer*)))
;;      (format t "~S~%" (mod ticks 10000))
      (when (< (mod ticks 10000) 1000)
	(format t "FPS: ~S~%" avg-fps)
	(format t "Frame ticks: ~S~%" frame-ticks)
	(format t "Frame length: ~S~%" (float *frame-length*)))
      (when (< frame-ticks *frame-length*)
	(format t "Delay: ~S~%" (float (- frame-ticks *frame-length*)))
	(sdl2:delay (round (- frame-ticks *frame-length*)))
	))) ;; 60 FPS hard coded
  )

(defun main ()
  ;; (bt:make-thread (lambda () (swank:create-server :port 4005
  ;; 						  :dont-close t)))
  (with-main
      (sdl2:with-init (:everything)
	(debug-log "Using SDL library version: ~D.~D.~D.~%"
		   sdl2-ffi:+sdl-major-version+
		   sdl2-ffi:+sdl-minor-version+
		   sdl2-ffi:+sdl-patchlevel+)
	(sdl2:with-window (win :w 640 :h 480 :title "Montana" :flags '(:shown :opengl))
	  (setf *window* win)
	  (sdl2:with-renderer (renderer win
					:index -1
					:flags
					'(:accelerated :presentvsync))
	    (setf *renderer* renderer)
	    (sdl2-image:init '(:png))
	    (sdl2-ttf:init)
	    (sdl2:start-text-input)
	    (setf *font* (sdl2-ttf:open-font *font-path* 28))
	    (setf *fps-counter-timer* (make-instance 'timer))
	    (timer-start *fps-counter-timer*)
	    (sdl2:with-event-loop (:method :poll)
	      (:quit () #'handle-quit)
	      (:keydown
	       (:keysym keysym :timestamp timestamp)
	       (handle-keydown keysym))
	      (:keyup
	       (:keysym keysym :timestamp timestamp)
	       (handle-keyup keysym))
	      (:windowevent
	       (:type type :data1 data1 :data2 data2 :timestamp timestamp)
	       (handle-windowevent type data1 data2))
	      (:textinput
	       (:text txt)
	       (debug-log "TEXT INPUT: ~S~%" txt))
	      (:textediting
	       (:length length :start start :text text :window-id
			window-id :timestamp timestamp :type type)
	       (debug-log "TEXT EDITING: ~S/~%S/~%S/~S/~S/~S~%"
			  length start text window-id timestamp type))
	      (:mousebuttondown
	       (:button button :x x :y y :timestamp timestamp)
	       (handle-mousebuttondown x y button timestamp))
	      (:mousebuttonup
	       (:button button :x x :y y :timestamp timestamp)
	       (handle-mousebuttonup x y button timestamp))
	      (:mousemotion
	       (:yrel yrel :xrel xrel :y y :x x :state state :which
		      which :timestamp timestamp)
	       (handle-mousemotion xrel yrel x y state which timestamp))
	      (:mousewheel
	       (:direction direction :y y :x x :which which :timestamp timestamp)
	       (handle-mousewheel x y which timestamp))
	      (:userevent
	       (:data1 data1 :data2 data2 :code code :timestamp timestamp)
	       (handle-userevent data1 data2 code timestamp))
	      (:idle ()
		     (do-idle win renderer)))
	    (sdl2-ttf:quit)
	    (sdl2-image:quit))))))
