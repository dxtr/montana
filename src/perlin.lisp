(in-package #:montana)

(defparameter *gradients1D*
  (make-array 2 :element-type 'single-float
	      :initial-contents '(1.0 -1.0)))

(defparameter *gradients2D*
  (make-array 8 :element-type 'vec2
	      :initial-contents
	      `(,(make-vec2 0.0 1.0)
		 ,(make-vec2 1.0 1.0)
		 ,(make-vec2 1.0 0.0)
		 ,(make-vec2 1.0 -1.0)
		 ,(make-vec2 0.0 -1.0)
		 ,(make-vec2 -1.0 -1.0)
		 ,(make-vec2 -1.0 0.0)
		 ,(make-vec2 -1.0 1.0)
		 )))
(defparameter *gradients2DMask*
  (- (length *gradients2D*) 1))

(defgeneric perlin-noise (point)
  (:documentation "Generate perlin noise for a specific point"))

(defmethod perlin-noise ((point single-float))
  '())

(defun smooth (p)
  (* p p p (+ (* p (- (* p 6) 15)) 10)))

(defun lerp (x a b)
  (+ a (* x (- b a))))

(defmethod perlin-noise ((point vec2))
  (let* ((point-floor (vector-floor point))
	 (xi (logand (round (vec-x point-floor)) #xFF))
	 (yi (logand (round (vec-y point-floor)) #xFF))
	 (pointf (make-vec2 (- (vec-x point)
			       (vec-x point-floor))
			    (- (vec-y point)
			       (vec-y point-floor))))
	 (points (make-vec2 (smooth (vec-x pointf))
			    (smooth (vec-y pointf))))
	 (g00 (aref *hash* (+ (aref *hash* yi) xi)))
	 (g10 (aref *hash* (+ (aref *hash* yi) xi 1)))
	 (g01 (aref *hash* (+ (aref *hash* (+ yi 1)) xi)))
	 (g11 (aref *hash* (+ (aref *hash* (+ yi 1)) xi 1)))
	 (n00 (vector-dot (aref *gradients2D* (logand g00
						      *gradients2DMask*))
			  (vec-x pointf)
			  (vec-y pointf)))
	 (n10 (vector-dot (aref *gradients2D* (logand g10
						      *gradients2DMask*))
			  (- (vec-x pointf) 1.0)
			  (vec-y pointf)))
	 (n01 (vector-dot (aref *gradients2D* (logand g01
						      *gradients2DMask*))
			  (vec-x pointf)
			  (- (vec-y pointf) 1.0)))
	 (n11 (vector-dot (aref *gradients2D* (logand g11
						      *gradients2DMask*))
			  (- (vec-x pointf) 1.0)
			  (- (vec-y pointf) 1.0))))
    (lerp (vec-y points)
	  (lerp (vec-x points) n00 n10)
	  (lerp (vec-x points) n01 n11))))
