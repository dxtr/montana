;;;; montana.asd

(asdf:defsystem #:montana
  :description "Describe montana here"
  :author "Kim Lidström <dxtr@emacs.church>"
  :license "ISC"
  :pathname "src"
  :depends-on (#:alexandria
	       #:bordeaux-threads
               #:sdl2
               #:sdl2-image
               #:sdl2-ttf
               #:cl-opengl)
  :serial t
  :components ((:file "package")
               (:file "montana")
               (:file "main")))


